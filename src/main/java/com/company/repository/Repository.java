package com.company.repository;

import com.company.dto.ListOfNumbers;

public interface Repository {

    ListOfNumbers getSudokuListOfNumbers();

}

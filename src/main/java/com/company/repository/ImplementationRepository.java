package com.company.repository;

import com.company.dto.ListOfNumbers;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Repository
public class ImplementationRepository implements Repository {

    private String file = "sudokuFiles/okSudoku.csv";
    private String sign = ";";
    public static final Integer SIZE_OF_ONE_SQUARE = 9;



    private List<List<Integer>> getValues() {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(file);
        List<List<Integer>> listOfNumbersSudoku = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String sudokuLine;
            while ((sudokuLine = br.readLine()) != null) {
                List<Integer> row = new ArrayList<>();
                String[] values = sudokuLine.split(sign);
                int rowSize = values.length;
                for (int x = 0; x < SIZE_OF_ONE_SQUARE; x++) {
                    if (x < rowSize && values[x] != null && !values[x].equals("")) {
                        row.add(Integer.parseInt(values[x]));
                    } else {
                        row.add(0);
                    }
                }
                listOfNumbersSudoku.add(row);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listOfNumbersSudoku;
    }
    @Override
    public ListOfNumbers getSudokuListOfNumbers() {
        List<List<Integer>> sudokuNumbers = getValues();
        return new ListOfNumbers(sudokuNumbers);
    }
}

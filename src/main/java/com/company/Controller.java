package com.company;

import com.company.dto.SudokuDto;
import com.company.service.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

    @Autowired
    private Service service;

    @CrossOrigin
    @PostMapping("/api/sudoku/verify")
    public ResponseEntity<SudokuDto> verifySudoku() {
        SudokuDto sudokuDto = service.checkSudoku();
        if (sudokuDto.checkValidation()) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(sudokuDto, HttpStatus.BAD_REQUEST);
        }

    }
}

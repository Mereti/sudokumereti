package com.company.service;

import com.company.dto.SudokuDto;
import com.company.dto.ListOfNumbers;
import com.company.repository.ImplementationRepository;
import com.company.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@org.springframework.stereotype.Service
public class ImplementationToCheck implements Service {
    @Autowired
    private Repository repository;

    private List<Integer> checkRows(List<List<Integer>> sudokuNumbers) {

        List<Integer> invalidIds = new ArrayList<>();
        int i = 0;
        for (List<Integer> row: sudokuNumbers) {
            List<Integer> fields = new ArrayList<>();
            for (Integer value: row) {
                if (fields.contains(value) && value != 0) {
                    invalidIds.add(i);
                } else {
                    fields.add(value);
                }
            }
            i++;
        }

        return invalidIds;
    }

    private List<Integer> checkKolumns(List<List<Integer>> sudokuNumbers) {

        List<Integer> invalidIds = new ArrayList<>();

        for (int i = 0; i < ImplementationRepository.SIZE_OF_ONE_SQUARE; i++) {
            List<Integer> columnElements = new ArrayList<>();
            for (int j = 0; j < ImplementationRepository.SIZE_OF_ONE_SQUARE; j++) {
                Integer value = sudokuNumbers.get(j).get(i);
                if (value != 0 && columnElements.contains(value)) {
                    invalidIds.add(j);
                } else {
                    columnElements.add(value);
                }
            }
        }

        return invalidIds;
    }

    private List<Integer> checkSquares(List<List<Integer>> sudokuNumbers) {
        List<List<Integer>> squares = squareSudoku(sudokuNumbers);
        return checkRows(squares);
    }

    private List<List<Integer>> squareSudoku(List<List<Integer>> squareSudokuList) {
        int sizeOfOneLine = ImplementationRepository.SIZE_OF_ONE_SQUARE/3;
        List<List<Integer>> squareList = new ArrayList<>();
        for (int i = 0; i < sizeOfOneLine; i++) {
            for (int j = 0; j < sizeOfOneLine; j++) {
                squareList.add(Arrays.asList(
                        squareSudokuList.get(i * sizeOfOneLine).get(j * sizeOfOneLine),
                        squareSudokuList.get(i * sizeOfOneLine).get(j * sizeOfOneLine + 1),
                        squareSudokuList.get(i * sizeOfOneLine).get(j * sizeOfOneLine + 2),
                        squareSudokuList.get(i * sizeOfOneLine + 1).get(j * sizeOfOneLine),
                        squareSudokuList.get(i * sizeOfOneLine + 1).get(j * sizeOfOneLine + 1),
                        squareSudokuList.get(i * sizeOfOneLine + 1).get(j * sizeOfOneLine + 2),
                        squareSudokuList.get(i * sizeOfOneLine + 2).get(j * sizeOfOneLine),
                        squareSudokuList.get(i * sizeOfOneLine + 2).get(j * sizeOfOneLine + 1),
                        squareSudokuList.get(i * sizeOfOneLine + 2).get(j * sizeOfOneLine + 2)
                ));
            }
        }
        return squareList;
    }


    @Override
    public SudokuDto checkSudoku() {
        ListOfNumbers listOfNumbers = repository.getSudokuListOfNumbers();
        List<List<Integer>> sudokuNumbers = listOfNumbers.getListOfNumberLists();
        List<Integer> incorrectRows = checkRows(sudokuNumbers);
        List<Integer> incorrectKolumns = checkKolumns(sudokuNumbers);
        List<Integer> incorrectSquares = checkSquares(sudokuNumbers);

        return new SudokuDto(incorrectRows, incorrectKolumns, incorrectSquares);
    }

}

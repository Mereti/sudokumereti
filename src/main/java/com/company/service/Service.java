package com.company.service;

import com.company.dto.SudokuDto;

public interface Service {

    SudokuDto checkSudoku();

}

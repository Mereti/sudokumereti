package com.company.dto;

import java.util.List;

public class SudokuDto {

    private List<Integer> lineIds;
    private List<Integer> kolumnIds;
    private List<Integer> areaIds;
    public SudokuDto(List<Integer> lineIds, List<Integer> kolumnIds, List<Integer> areaIds) {
        this.lineIds = lineIds;
        this.kolumnIds = kolumnIds;
        this.areaIds = areaIds;
    }
    public List<Integer> getLineIds() {
        return lineIds;
    }
    public void setLineIds(List<Integer> lineIds) {
        this.lineIds = lineIds;
    }
    public List<Integer> getKolumnIds() {
        return kolumnIds;
    }
    public void setKolumnIds(List<Integer> kolumnIds) {
        this.kolumnIds = kolumnIds;
    }
    public List<Integer> getAreaIds() {
        return areaIds;
    }
    public void setAreaIds(List<Integer> areaIds) {
        this.areaIds = areaIds;
    }
    public boolean checkValidation() {
        if (this.areaIds.size() > 0 || this.kolumnIds.size() > 0 || this.lineIds.size() > 0) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return "SudokuDto{" +
                "lineIds=" + lineIds +
                ", kolumnIds=" + kolumnIds +
                ", areaIds=" + areaIds +
                '}';
    }
}

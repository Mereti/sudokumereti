package com.company.dto;

import java.util.List;

public class ListOfNumbers {
    private List<List<Integer>> listOfNumberLists;

    public ListOfNumbers(List<List<Integer>> listOfNumberLists) {
        this.listOfNumberLists = listOfNumberLists;
    }

    public List<List<Integer>> getListOfNumberLists() {
        return listOfNumberLists;
    }

}
